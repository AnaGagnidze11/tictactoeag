package com.example.tictactoeag

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var firstPlayer = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button00.setOnClickListener {
            clickButton(button00)
        }
        button01.setOnClickListener {
            clickButton(button01)
        }
        button02.setOnClickListener {
            clickButton(button02)
        }
        button10.setOnClickListener {
            clickButton(button10)
        }
        button11.setOnClickListener {
            clickButton(button11)
        }
        button12.setOnClickListener {
            clickButton(button12)
        }
        button20.setOnClickListener {
            clickButton(button20)
        }
        button21.setOnClickListener {
            clickButton(button21)
        }
        button22.setOnClickListener {
            clickButton(button22)
        }

    }
    private fun clickButton(button: Button){
        if (firstPlayer) {
            button.text = "X"
        } else {
            button.text = "0"
        }
        button.isClickable = false
        firstPlayer = !firstPlayer
        whoWon()
        clickToRestart()
    }
    private fun whoWon() {
        if (button00.text.toString().isNotEmpty() && button00.text.toString() == button01.text.toString() && button01.text.toString() == button02.text.toString()) {
            showToast(button00.text.toString())
            button10.isClickable = false
            button11.isClickable = false
            button12.isClickable = false
            button20.isClickable = false
            button21.isClickable = false
            button22.isClickable = false
        } else if (button10.text.toString().isNotEmpty() && button10.text.toString() == button11.text.toString() && button11.text.toString() == button12.text.toString()) {
            showToast(button10.text.toString())
            button00.isClickable = false
            button01.isClickable = false
            button02.isClickable = false
            button20.isClickable = false
            button21.isClickable = false
            button22.isClickable = false
        } else if (button20.text.toString().isNotEmpty() && button20.text.toString() == button21.text.toString() && button21.text.toString() == button22.text.toString()) {
            showToast(button20.text.toString())
            button00.isClickable = false
            button01.isClickable = false
            button02.isClickable = false
            button10.isClickable = false
            button11.isClickable = false
            button12.isClickable = false
        } else if (button00.text.toString().isNotEmpty() && button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString()) {
            showToast(button00.text.toString())
            button01.isClickable = false
            button02.isClickable = false
            button11.isClickable = false
            button12.isClickable = false
            button21.isClickable = false
            button22.isClickable = false
        } else if (button01.text.toString().isNotEmpty() && button01.text.toString() == button11.text.toString() && button11.text.toString() == button21.text.toString()) {
            showToast(button01.text.toString())
            button00.isClickable = false
            button02.isClickable = false
            button10.isClickable = false
            button12.isClickable = false
            button20.isClickable = false
            button22.isClickable = false
        } else if (button02.text.toString().isNotEmpty() && button02.text.toString() == button12.text.toString() && button12.text.toString() == button22.text.toString()) {
            showToast(button02.text.toString())
            button00.isClickable = false
            button01.isClickable = false
            button10.isClickable = false
            button11.isClickable = false
            button20.isClickable = false
            button21.isClickable = false
        } else if (button00.text.toString().isNotEmpty() && button00.text.toString() == button11.text.toString() && button11.text.toString() == button22.text.toString()) {
            showToast(button00.text.toString())
            button01.isClickable = false
            button02.isClickable = false
            button10.isClickable = false
            button12.isClickable = false
            button20.isClickable = false
            button21.isClickable = false
        } else if (button02.text.toString().isNotEmpty() && button02.text.toString() == button11.text.toString() && button11.text.toString() == button20.text.toString()) {
            showToast(button02.text.toString())
            button00.isClickable = false
            button01.isClickable = false
            button10.isClickable = false
            button12.isClickable = false
            button21.isClickable = false
            button22.isClickable = false
        } else if (button00.text.toString().isNotEmpty() && button01.text.toString()
                .isNotEmpty() && button02.text.toString().isNotEmpty() && button10.text.toString()
                .isNotEmpty() && button11.text.toString().isNotEmpty() && button12.text.toString()
                .isNotEmpty() && button20.text.toString().isNotEmpty() && button21.text.toString()
                .isNotEmpty() && button22.text.toString().isNotEmpty()) {
            whenItsTie("It's a Tie!")
        }

}
    private fun showToast(message:String){
        Toast.makeText(this,"$message won!", Toast.LENGTH_SHORT).show()

    }

    private fun whenItsTie(message: String) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


    private fun clickToRestart(){
        resetButton.setOnClickListener {
            button00.text = ""
            button01.text = ""
            button02.text = ""
            button10.text = ""
            button11.text = ""
            button12.text = ""
            button20.text = ""
            button21.text = ""
            button22.text = ""

            init()

        }

    }
}